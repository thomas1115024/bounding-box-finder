#include <random>
#include <iostream>
#include <chrono>
#include <set>
#include <vector>
#include <cstring>
#include <algorithm>
#include <chrono>
#include <queue>

#define float double

struct BoundingBox {

  float x_left_;
  float y_top_;
  float x_right_;    // x_right > x_left
  float y_bottom_;   // y_bottom > y_top
  float bbox_area_;

  BoundingBox() {
    x_left_ = y_top_ = x_right_ = y_bottom_ = bbox_area_ = 0;
  }

  float x_left() { return x_left_; }
  float y_top() { return y_top_; }
  float x_right() { return x_right_; }
  float y_bottom() { return y_bottom_; }
  float bbox_area() { return bbox_area_; }

  void set_x_left(float x){ x_left_ = x; }
  void set_y_top(float x){ y_top_ = x; }
  void set_x_right(float x){ x_right_ = x; }
  void set_y_bottom(float x){ y_bottom_ = x; }
  void set_bbox_area(float x){ bbox_area_ = x; }

};

struct motionRegion {

  int region_id_;
  BoundingBox bbox_;
  float confidence_;
  std::string notes_;

  motionRegion() {
    region_id_ = 0;
    confidence_ = 0.0;
    notes_ = "";
  }

};

struct DisjointSetUnion {
  int n;
  std::vector<int> par;
  std::vector<int> rk;

  int find_parent(int x) {
    if(x == par[x]) return x;
    return par[x] = find_parent(par[x]);
  }

  //set x's parent to y
  void set_parent(int x, int y) {
    par[x] = y;
  }

  bool merge(int x, int y) {
    x = find_parent(x);
    y = find_parent(y);
    if(x == y) return false;
    if(rk[x] < rk[y]) std::swap(x, y);
    par[y] = x;
    if(rk[x] == rk[y]) rk[x]++;
    return true;
  }

  DisjointSetUnion(){}
  DisjointSetUnion(int n_) {
    n = n_;
    rk.resize(n, 0);
    par.resize(n);
    for(int i = 0; i < n; i++) par[i] = i;
  }
};

//Supports range chmax updates (a = max(a, b)) and range max queries
struct SegmentTree {
  int n;
  std::vector<std::set<std::pair<float, int>, std::greater<std::pair<float, int>>>> node_mx;
  std::vector<std::pair<float, int>> subtree_mx;


  SegmentTree() {}
  SegmentTree(int n_) {
    n = n_;
    node_mx.resize(4*n);
    subtree_mx.resize(4*n);
    for(int i = 0; i < 4*n; i++) {
      node_mx[i].insert(std::make_pair(-1.0, -1));
      subtree_mx[i] = std::make_pair(-1.0, -1);
    }
  }

  void upd(int l, int r, std::pair<float, int> v, int t, int ul, int ur, int cur) {
    if(l <= ul && ur <= r) {
      if(t) {
        node_mx[cur].insert(v);
      } else {
        node_mx[cur].erase(v);
      }
      subtree_mx[cur] = *node_mx[cur].begin();
      if(ul != ur) {
        subtree_mx[cur] = std::max({subtree_mx[cur], subtree_mx[cur*2 + 1], subtree_mx[cur*2 + 2]});
      }
      return;
    }
    int mid = (ul + ur)/2;
    if(l <= mid) upd(l, r, v, t, ul, mid, cur*2 + 1);
    if(r > mid) upd(l, r, v, t, mid + 1, ur, cur*2 + 2);
    subtree_mx[cur] = std::max({*node_mx[cur].begin(), subtree_mx[cur*2 + 1], subtree_mx[cur*2 + 2]});
  }

  //range chmax
  //a = max(a, b)
  void update(int l, int r, std::pair<float, int> v) {
    upd(l, r, v, 1, 0, n - 1, 0);
  }

  void undo_update(int l, int r, std::pair<float, int> v) {
    upd(l, r, v, 0, 0, n - 1, 0);
  }

  std::pair<float, int> que(int l, int r, int ul, int ur, int cur) {
    if(l <= ul && ur <= r) {
      return subtree_mx[cur];
    }
    int mid = (ul + ur)/2;
    if(r <= mid) return std::max(que(l, r, ul, mid, cur*2 + 1), *node_mx[cur].begin());
    if(l > mid) return std::max(que(l, r, mid + 1, ur, cur*2 + 2), *node_mx[cur].begin());
    return std::max({que(l, r, ul, mid, cur*2 + 1), que(l, r, mid + 1, ur, cur*2 + 2), *node_mx[cur].begin()});
  }

  std::pair<float, int> query(int l, int r) {
    return que(l, r, 0, n - 1, 0);
  }
};

BoundingBox CombineBoundingBoxes(BoundingBox& a, BoundingBox& b) {
  BoundingBox bbox;
  bbox.set_x_left(std::min(a.x_left(), b.x_left()));
  bbox.set_x_right(std::max(a.x_right(), b.x_right()));
  bbox.set_y_top(std::min(a.y_top(), b.y_top()));
  bbox.set_y_bottom(std::max(a.y_bottom(), b.y_bottom()));
  bbox.set_bbox_area((bbox.x_right() - bbox.x_left()) * (bbox.y_bottom() - bbox.y_top()));
  return bbox;
}

float ComputeIntersection(BoundingBox& a, BoundingBox &b) {
  return std::max(float(0.0), std::min(a.x_right(), b.x_right()) - std::max(a.x_left(), b.x_left()))
                        * std::max(float(0.0), std::min(a.y_bottom(), b.y_bottom()) - std::max(a.y_top(), b.y_top()));
}

float ComputeUnion(BoundingBox& a, BoundingBox &b) {
  return (std::max(a.x_right(), b.x_right()) - std::min(a.x_left(), b.x_left()))
                        * (std::max(a.y_bottom(), b.y_bottom()) - std::min(a.y_top(), b.y_top()));
}

/*
** Finds a bbox such that it is within range, at least as large as the frame, and covers bbox
** frame_w and frame_h are the dimensions of the frame
** dir is which corner of range it tries to shift the frame toward
** 0 - top left
** 1 - top right
** 2 - bottom left
** 3 - bottom right
** 4 - center
*/
BoundingBox FitBox(BoundingBox bbox, BoundingBox& range, float frame_w, float frame_h) {
  float w = std::max(frame_w, bbox.x_right() - bbox.x_left());
  float h = std::max(frame_h, bbox.y_bottom() - bbox.y_top());
  w = std::max(w, h*frame_w/frame_h);
  h = w*frame_h/frame_w;

  BoundingBox ret;
  float shift_x = 0.0, shift_y = 0.0;

  float x_mid = (bbox.x_left() + bbox.x_right())/2;
  ret.set_x_left(x_mid - w/2);
  ret.set_x_right(x_mid + w/2);
  //if(ret.x_right() > range.x_right()) shift_x = ret.x_right() - range.x_right();
  //if(ret.x_left() < range.x_left()) shift_x = range.x_left() - ret.x_left();

  float y_mid = (bbox.y_top() + bbox.y_bottom())/2;
  ret.set_y_top(y_mid - h/2);
  ret.set_y_bottom(y_mid + h/2);
  //if(ret.y_bottom() > range.y_bottom()) shift_y = ret.y_bottom() - range.y_bottom();
  //if(ret.y_top() < range.y_top()) shift_y = range.y_top() - ret.y_top();

  ret.set_x_left(std::max(range.x_left(), ret.x_left() + shift_x));
  ret.set_x_right(std::min(range.x_right(), ret.x_right() + shift_x));
  ret.set_y_top(std::max(range.y_top(), ret.y_top() + shift_y));
  ret.set_y_bottom(std::min(range.y_bottom(), ret.y_bottom() + shift_y));
  ret.set_bbox_area((ret.x_right() - ret.x_left()) * (ret.y_bottom() - ret.y_top()));
  return ret;
}

/*
** frame - the desired dimensions of the region.
** min_loss - the point at which to no longer split
** padding - the amount of padding
** Algorithm:
** Greedily split the set of points until splitting gives a worse value than minimum_loss. For each group of points, choose a pivot in X or Y to split by. If it is valid,
** split and continue the process. Always take the best split from the given sets. 
** Chooses a pivot in either X or Y to split each set by
** O(N log^2 N)
*/
std::vector<BoundingBox> CalculateMotionROI(std::vector<BoundingBox>& regions, float frame_w, float frame_h, float padding_w, float padding_h,
                                              float min_loss) {
  frame_w += 2*padding_w;
  frame_h += 2*padding_h;
  float aspect_ratio = frame_w/frame_h;
  int n = regions.size();
  BoundingBox border;
  border.set_x_left(0.0);
  border.set_x_right(1.0);
  border.set_y_top(0.0);
  border.set_y_bottom(1.0);
  border.set_bbox_area(1.0);
  //return Split(regions, border, frame_w, frame_h, min_loss);
  return regions;
}

/* 
** Removes all intersections
** Algorithm:
** Add in rectangles by increasing x_left, and merge all intersections. There will only be at most N - 1 merges in total.
** To do the merge, find the largest x_right in the set of added rectangles that cover some y coordinate i, such that y_top <= i < y_bottom
** Maintain this value on a segtree, and inserting a new rectangle becomes setting max_x_right[i] = max(max_x_right[i], x_right).
** After merging two rectangles, we want to undo the update from them, and we can do this by removing the lazy propagation tags from the segtree.
** Repeatedly find the largest x_right such that it intersects and remove it
** Assumes rectangles are [x_left, x_right) x [y_top, y_bottom)
** Uses the 
** O(N log^2 N)
*/
std::vector<BoundingBox> RemoveIntersection(std::vector<BoundingBox>& input_points) {
  int n = input_points.size();
  std::vector<float> y_coords(2*n);
  for(int i = 0; i < n; i++) {
    y_coords[2*i] = input_points[i].y_top();
    y_coords[2*i + 1] = input_points[i].y_bottom();
  }
  sort(y_coords.begin(), y_coords.end());
  y_coords.resize(unique(y_coords.begin(), y_coords.end()) - y_coords.begin());
  SegmentTree segtree(y_coords.size());
  std::vector<BoundingBox> bboxes = input_points; 
  sort(bboxes.begin(), bboxes.end(), [](BoundingBox& a, BoundingBox& b) {
    return a.x_left() < b.x_left();
  });
  std::vector<std::pair<int, int>> y_range(n);
  for(int i = 0; i < n; i++) {
    y_range[i].first = lower_bound(y_coords.begin(), y_coords.end(), bboxes[i].y_top()) - y_coords.begin();
    y_range[i].second = lower_bound(y_coords.begin(), y_coords.end(), bboxes[i].y_bottom()) - y_coords.begin() - 1; //make the range inclusive
  }
  std::vector<int> par(n);
  for(int i = 0; i < n; i++) {
    //rectangle with the largest x_right that could possibly intersect i
    std::pair<float, int> next_rectangle = segtree.query(y_range[i].first, y_range[i].second);
    par[i] = i;
    //while it does intersect, keep finding the next rectangle
    while(next_rectangle.first > bboxes[i].x_left()) {
      bboxes[i] = CombineBoundingBoxes(bboxes[i], bboxes[next_rectangle.second]);
      y_range[i].first = std::min(y_range[i].first, y_range[next_rectangle.second].first);
      y_range[i].second = std::max(y_range[i].second, y_range[next_rectangle.second].second);
      par[next_rectangle.second] = i;
      //undo the update because the rectangle no longer exists
      segtree.undo_update(y_range[next_rectangle.second].first, y_range[next_rectangle.second].second, 
                            std::make_pair(bboxes[next_rectangle.second].x_right(), next_rectangle.second));
      next_rectangle = segtree.query(y_range[i].first, y_range[i].second);
    }
    segtree.update(y_range[i].first, y_range[i].second, std::make_pair(bboxes[i].x_right(), i));
  }
  std::vector<BoundingBox> ret;
  for(int i = 0; i < n; i++) {
    if(par[i] == i) {
      ret.push_back(bboxes[i]);
    }
  }
  return ret;
}


/* 
** min_box_cost     - the minimum cost to consider a box
** min_cost         - the minimum cost needed for a group to be kept.
** diagonal_merging - whether or not to consider diagonal as adjacent
** padding          - how much padding to add
** Merges all points within some distance of each other into a single bounding box
** Algorithm:
** Break up the grid into rectangles of size box_w and box_h, and assign each input point to its box. If two adjacent boxes both contain points, merge them into one.
** Approximates each bounding box to its upper left corner, so try to set the box dimensions to be larger than the given bounding boxes
** O(N log N)
*/
std::vector<BoundingBox> GroupBoxes(std::vector<BoundingBox>& input_points, const std::vector<float>& input_costs, 
                                      float box_w, float box_h, float min_box_cost, float min_cost, float padding_w, 
                                      float padding_h, int diagonal_merging = 1) {
  int n = input_points.size();
  std::vector<std::pair<BoundingBox, float>> bboxes(n);
  for(int i = 0; i < n; i++){
    bboxes[i] = std::make_pair(input_points[i], input_costs[i]);
  }
  DisjointSetUnion dsu(n);
  sort(bboxes.begin(), bboxes.end(), [box_w](std::pair<BoundingBox, float>& a, std::pair<BoundingBox, float>& b) {
    if(std::floor(a.first.x_left()/box_w) == std::floor(b.first.x_left()/box_w)) return a.first.y_top() < b.first.y_top();
    return a.first.x_left() < b.first.x_left();
  });
  //Decompose the grid into boxes, and assign each point to its box
  int left_bound = 0;
  //stores the active boxes and their respective indices from the previous layer of boxes
  std::vector<int> prv_bboxes;
  std::vector<int> prv_indeces;
  for(int i = 0; i < n; i++) {
    //if the previous layer of boxes was more than one box away, we won't need to merge them
    int cur_bound = std::floor(bboxes[i].first.x_left()/box_w);
    if(cur_bound - left_bound > 1){
      prv_bboxes.clear();
      prv_indeces.clear();
    }
    //update current layer position
    //everything in this layer is guranteed to be sorted by y
    left_bound = cur_bound;
    int j = i;
    while(j < n && std::floor(bboxes[j].first.x_left()/box_w) <= left_bound) j++;
    //layer is guranteed to be sorted by y because of other sort function
    int prv_index = -1;
    int up_bound = 0;
    std::vector<int> new_bboxes;
    std::vector<int> new_indeces;
    for(int cur_index = i; cur_index < j; cur_index++) {
      //run a similar process downwards to merge all boxes in the current layer
      int new_bound = std::floor(bboxes[cur_index].first.y_top()/box_h);

      float cur_cost = bboxes[cur_index].second;
      int original_index = cur_index;
      while(cur_index + 1 < j && std::floor(bboxes[cur_index + 1].first.y_top()/box_h) == new_bound){
        cur_cost += bboxes[cur_index + 1].second;
        dsu.merge(cur_index, cur_index + 1);
        cur_index++;
      }

      //ignore it if the cell is too sparse
      if(cur_cost < min_box_cost) continue;

      if(new_bound - up_bound == 1 && original_index > i) {
        dsu.merge(original_index, original_index - 1);
      }
      up_bound = new_bound;
      new_bboxes.push_back(up_bound);
      new_indeces.push_back(cur_index);

      if(prv_index >= 0 && abs(up_bound - prv_bboxes[prv_index]) <= diagonal_merging) {
          dsu.merge(prv_indeces[prv_index], cur_index);
      }
      //merge with previous layer if applicable
      while(prv_index + 1 < prv_bboxes.size() && prv_bboxes[prv_index + 1] <= up_bound + diagonal_merging) { 
        prv_index++;   
        if(abs(up_bound - prv_bboxes[prv_index]) <= diagonal_merging) {
          dsu.merge(prv_indeces[prv_index], cur_index);
        }
      }
    }
    prv_bboxes = move(new_bboxes);
    prv_indeces = move(new_indeces);
    i = j - 1;
  }
  //combine merged components
  for(int i = 0; i < n; i++) {
    int par = dsu.find_parent(i);
    if(par != i) {
      bboxes[par].first = CombineBoundingBoxes(bboxes[par].first, bboxes[i].first);
      bboxes[par].second += bboxes[i].second;
    }
  }
  //erase all components with not enough cost
  std::vector<BoundingBox> ret;
  for(int i = 0; i < n; i++) {
    if(dsu.find_parent(i) == i && bboxes[i].second >= min_cost) {
      bboxes[i].first.set_x_left(std::max(float(0.0), bboxes[i].first.x_left() - padding_w));
      bboxes[i].first.set_x_right(std::min(float(1.0), bboxes[i].first.x_right() + padding_w));
      bboxes[i].first.set_y_top(std::max(float(0.0), bboxes[i].first.y_top() - padding_h));
      bboxes[i].first.set_y_bottom(std::min(float(1.0), bboxes[i].first.y_bottom() + padding_h));
      bboxes[i].first.set_bbox_area((bboxes[i].first.x_right() - bboxes[i].first.x_left()) * (bboxes[i].first.y_bottom() - bboxes[i].first.y_top()));
      ret.push_back(bboxes[i].first);
    }
  }
  return ret;
}

/*
** motion_points     - bounding boxes that are changing
** prv_roi           - bounding boxes that the were identified as useful in the past iteration
** roi_cost          - cost to assign to the previous regions of interest
** prv_ignore        - bounding boxes that were used in the past iteration
** ignore_cost       - cost to assign to the previous ignored regions 
** probability_map   - map of probability regions
** probability_costs - cost of each probability region
** base_cost         - base cost of a single region
** cost of point = probability_costs*base_cost + (roi_cost + ignore_cost)*(# of roi that cover it) - ignore_cost*(# of ignored regions that cover it)
** the prv_roi should counterract the prv_ignore since it should always be inside
** O((prv_roi + prv_ignore + probability_ap) * motion_points)
*/
std::pair<std::vector<BoundingBox>, std::vector<float>> AssignCosts(std::vector<BoundingBox>& motion_points, 
      std::vector<BoundingBox>& prv_roi, float roi_cost, std::vector<BoundingBox>& prv_ignore, 
      float ignore_cost, std::vector<BoundingBox>& probability_map, std::vector<float>& probability_costs,
      float base_cost) {
  int n = motion_points.size();
  std::vector<float> costs(n, base_cost);
  //TODO: make this loop faster
  for(int i = 0; i < probability_map.size(); i++) {
    for(int j = 0; j < n; j++) {
      if(ComputeIntersection(probability_map[i], motion_points[j]) > 0.0) {
        costs[j] *= probability_costs[i];
      }
    }
  }
  for(int i = 0; i < prv_roi.size(); i++) {
    for(int j = 0; j < n; j++) {
      if(ComputeIntersection(prv_roi[i], motion_points[j]) > 0.0) {
        costs[j] += roi_cost + ignore_cost;
      }
    }
  }
  for(int i = 0; i < prv_ignore.size(); i++) {
    for(int j = 0; j < n; j++) {
      if(ComputeIntersection(prv_ignore[i], motion_points[j]) > 0.0) {
        costs[j] -= ignore_cost;
      }
    }
  }
  return std::make_pair(motion_points, costs);
}

std::vector<BoundingBox> ComputeROI(
        std::vector<BoundingBox>& motion_regions, //regions of motion
        std::vector<BoundingBox>& prv_roi, //previous ai detected bboxes
        std::vector<BoundingBox>& prv_bboxes, //previous chosen bboxes
        std::vector<BoundingBox>& probability_map, //probability map regions
        std::vector<float>& probability_costs, //probability values of probability map
        float frame_w, float frame_h, //dimensions of frame needed for model
        float box_w, float box_h, //dimensions of box when segmenting
        float min_loss, //minimum area saved needed to consider a split
        float base_cost, //base cost assigned to a normal motion point
        float roi_cost, //added cost for previous regions of interest
        float ignore_cost, //subtracted cost for previous bounding boxes
        float min_per_box, //minimum number of normal points needed for a segment to be considered when segmenting
        float min_per_group //minimum number of normal points needed for a group to be considered after grouping
      ) {

  std::pair<std::vector<BoundingBox>, std::vector<float>> regions = AssignCosts(motion_regions, 
                                                                  prv_roi, roi_cost, prv_bboxes, ignore_cost, 
                                                                  probability_map, probability_costs, base_cost);

  std::vector<BoundingBox> bboxes = GroupBoxes(regions.first, regions.second, box_w, box_h, min_per_box*base_cost,
                                                min_per_group*base_cost, box_w/2, box_h/2, 0);
  BoundingBox screen;
  screen.set_x_left(0.0);
  screen.set_x_right(1.0);
  screen.set_y_top(0.0);
  screen.set_y_bottom(1.0);
  screen.set_bbox_area(1.0);
  //TODO: optimize this loop
  for(int i = 0; i < bboxes.size(); i++) bboxes[i] = FitBox(bboxes[i], screen, frame_w, frame_h);
  std::vector<BoundingBox> new_bboxes = RemoveIntersection(bboxes);
  while(new_bboxes.size() < bboxes.size()) {
    bboxes = move(new_bboxes);
    for(int i = 0; i < bboxes.size(); i++) bboxes[i] = FitBox(bboxes[i], screen, frame_w, frame_h);
    new_bboxes = RemoveIntersection(bboxes);
  }
  return bboxes;
}



int main(){
  std::freopen("map.txt", "r", stdin);
  std::freopen("box_map.txt", "w", stdout);
  int n, m;
  std::cin >> n >> m;
  std::vector<BoundingBox> points;
  std::vector<float> costs;
  std::vector<std::vector<char>> arr(n, std::vector<char>(m));
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      std::cin >> arr[i][j];
      if(arr[i][j] == 'X'){
        BoundingBox cur;
        cur.set_x_left(float(j)/m);
        cur.set_x_right(float(j + 1)/m);
        cur.set_y_top(float(i)/n);
        cur.set_y_bottom(float(i + 1)/n);
        cur.set_bbox_area((cur.x_right() - cur.x_left())*(cur.y_bottom() - cur.y_top()));
        points.push_back(cur);
        costs.push_back(1);
      }
    }
  }
  int lim = std::min((int)points.size(), 3);
  {
    auto start = std::chrono::high_resolution_clock::now();
    std::vector<BoundingBox> empty_bbox;
    std::vector<float> empty_float;
    std::vector<BoundingBox> bboxes = ComputeROI(points, empty_bbox, empty_bbox, empty_bbox, empty_float, 
                                                  float(5)/m, float(1)/n, float(2)/m, float(2)/n, 0,
                                                  1.0, 2.0, 1.0, 1.0, 40.0);
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "Runtime 1: " << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << "\n";
    {
      std::vector<std::vector<char>> outp = arr;
      for(int j = 0; j < bboxes.size(); j++){
        for(int k = int(bboxes[j].x_left()*m); k < int(bboxes[j].x_right()*m); k++){
          outp[int(bboxes[j].y_top()*n)][k] = outp[int(bboxes[j].y_bottom()*n) - 1][k] = 'D';
        }
        for(int k = int(bboxes[j].y_top()*n); k < int(bboxes[j].y_bottom()*n); k++){
          outp[k][int(bboxes[j].x_left()*m)] = outp[k][int(bboxes[j].x_right()*m) - 1] = 'D';
        } 
      }
      for(int j = 0; j < n; j++){
        for(int k = 0; k < m; k++){
          std::cout << outp[j][k];
        }
        std::cout << "\n";
      }
    }
  }
}
