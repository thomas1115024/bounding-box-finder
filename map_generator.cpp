#include <random>
#include <iostream>
#include <chrono>
#include <set>
#include <vector>
#include <cstring>
using namespace std;

#define pb push_back
#define ff first
#define ss second

typedef long long ll;
typedef long double ld;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef pair<ld, ld> pld;

const int INF = 1e9;
const ll LLINF = 1e18;
const int MOD = 1e9 + 7;

inline ll ceil0(ll a, ll b) {
    return a / b + ((a ^ b) > 0 && a % b);
}

void setIO() {
    ios_base::sync_with_stdio(0); cin.tie(0);
}

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
inline int random(int l, int r) { return uniform_int_distribution<int>(l, r)(rng); }
set<pii> points;

void generate(int x1, int y1, int x2, int y2, int cnt){
  vector<pii> v;
  for(int i = x1; i <= x2; i++){
    for(int j = y1; j <= y2; j++){
      v.pb({i, j});
    }
  }
  
  cnt = min(cnt, (int)v.size());
  while(cnt--){
    int ind = random(0, v.size() - 1);
    points.insert(v[ind]);
    v.erase(v.begin() + ind);
  }
}

int main(){
  setIO();
  freopen("map_stats.txt", "r", stdin);
  freopen("map.txt", "w", stdout);
  int n, m;
  cin >> n >> m;
  generate(40, 50, 60, 60, 150);
  generate(1, 1, 30, 30, 100);
  generate(40, 1, 60, 11, 150);
  generate(24, 62, 39, 100, 150);
  generate(22, 28, 32, 71, 150);
  generate(1, 100, 1, 100, 1);
  int arr[n + 1][m + 1];
  memset(arr, 0, sizeof(arr));
  for(pii i : points) arr[i.ff][i.ss] = 1;
  cout << n << " " << m << endl;
  for(int i = 1; i <= n; i++){
    for(int j = 1; j <= m; j++){
      if(arr[i][j]) cout << 'X';
      else cout << '.';
    }
    cout << endl;
  }
}
